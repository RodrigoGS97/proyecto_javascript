# Proyecto del curso de Javascript
## Descripción
Reutilizamos el codigo del profesor en la parte del index, solo que el primer 
parametro del ajax lo cambiamos para que usara la función search de la api y que 
devolviera las peliculas relacionadas con la busqueda. Aunque utiliza el codigo 
proporcionado en el index se muestran las tarjetas, una por renglon, esto porque 
estan dentro de una lista. 
## Autor
* González Sánchez Rodrigo
### Contacto
rodrigogonsan1997@gmail.com